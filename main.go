package main

import (
  "os"
  "log"
  "fmt"
  "flag"
  "time"
  "bytes"
  "strings"
  "net/url"
  "net/http"
  "io/ioutil"
  "github.com/kurrik/oauth1a"
  "github.com/kurrik/twittergo"
)

func LoadCreds() (client *twittergo.Client, err error) {
  creds, err := ioutil.ReadFile("CREDENTIALS.tu")
  if err != nil {
    panic(err)
  }
  log.Print("\033[38:2:150:150:0mCredentials file loaded.\033[0m")
  lines := strings.Split(string(creds), "\n")

  conf := &oauth1a.ClientConfig{
    ConsumerKey:  lines[0],
    ConsumerSecret:  lines[1],
  }
  usr := oauth1a.NewAuthorizedConfig(lines[2], lines[3])
  client = twittergo.NewClient(conf, usr)
  return client, err
}

type Arguments struct {
  ScreenN string
  OutputF string
}

func parseArguments() *Arguments {
  a := &Arguments{}
  flag.StringVar(&a.ScreenN, "screen_name", "twitterapi", "Screen name")
  flag.StringVar(&a.OutputF, "out", "output.json", "Output file")
  return a
}



func main() {
  if len(os.Args) < 2 || len(os.Args) > 2 {
    log.Print("\033[38:2:255:0:0mWRONG NUMBER OF ARGUMENTS PASSED\033[0m")
    log.Print("\033[38:2:0:255:0mUsage is `./tweetus 'STATUS'`\033[0m")
    os.Exit(1)
  }
  var buffer bytes.Buffer
  log.New(&buffer, "tweetus", log.Llongfile)
  log.Print("\033[38:2:150:150:0mLet's tweetus, cleetus!\033[0m")
  //Declaration block
  var (
    err  error
    client  *twittergo.Client
    req  *http.Request
    resp  *twittergo.APIResponse
    args  *Arguments
    out  *os.File
    query  url.Values
  )


  //Start consuming the variables
  args = parseArguments()
  client, err = LoadCreds()
  if err != nil {
    panic(err)
  }

  out, err = os.Create(args.OutputF)
  if err != nil {
    panic(err)
  }
  defer out.Close()

  //List the api endpoints needed
  const (
    count int = 200
    urllist  = "/1.1/statuses/update.json?%v"

    minWait = time.Duration(10) *time.Second
  )

  //Start the query setup
  query = url.Values{}
  query.Set("status", fmt.Sprintf("%v", os.Args[1]))

  //Start the main loop
  endpoint := fmt.Sprintf(urllist, query.Encode())
  req, err = http.NewRequest("POST", endpoint, nil)
  if err != nil {
    panic(err)
  }
  log.Print("\033[38:50:50:200mTweeting...\033[0m")

  resp, err = client.SendRequest(req)
  if err != nil {
    panic(err)
  }
  log.Print("\033[38:150:150:0mTweet response "+resp.Status+"\033[0m")





}
